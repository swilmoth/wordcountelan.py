This is a simple Python script to count transcribed words in an ELAN file.
It counts (space-delimited) words on tiers with the tier type "transcription".
To run over many files, you can use a command such as:
`for file in */*.eaf ; do python wordcountELAN.py $file >> count.txt ; done`

The modified version `wordcountELANbySpeaker.py` gives a breakdown for each participant in each file, as well as a total for each file. Tiers missing participants are printed to the terminal.