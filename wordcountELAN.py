#!/usr/bin/env python
# -*- coding: utf8 -*-
"""
count words in elan file
tier type = "transcription"

example command:
for file in */*.eaf ; do python wordcountELAN.py $file ; done

by Sasha Wilmoth, 2019
"""

import sys, codecs, re
from argparse import ArgumentParser,RawTextHelpFormatter

sys.stdout=codecs.getwriter('utf8')(sys.stdout)
sys.stderr=codecs.getwriter('utf8')(sys.stderr)

def main():
	parser = ArgumentParser(description = __doc__, formatter_class=RawTextHelpFormatter)
	parser.add_argument('input', help = 'elan file')
	opts = parser.parse_args()

	txtier = False
	wordcount = 0

	for line in codecs.open(opts.input, 'r', 'utf8'):
		line = line.strip('\n')
		if "<TIER " in line:
			# change this value if you want a different tier type
			if 'LINGUISTIC_TYPE_REF="transcription"' in line:
				txtier = True
			else:
				txtier = False

		if txtier:
			if "ANNOTATION_VALUE" in line:
				regex = re.search('<ANNOTATION_VALUE>(.*)<\/ANNOTATION_VALUE',line)
				if regex:
					utt = regex.group(1)
					wordcount += len(utt.split(' '))

	print(sys.argv[1])
	print(str(wordcount))
	print

if __name__=="__main__":
	main()
