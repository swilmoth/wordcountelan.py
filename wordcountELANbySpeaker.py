#!/usr/bin/env python
# -*- coding: utf8 -*-
"""
count words in elan file
tier type = "transcription"

example command:
for file in */*.eaf ; do python wordcountELAN.py $file ; done

This version of the script gives a breakdown by speaker as well as a total. Any tiers without participants are printed to the terminal.

by Sasha Wilmoth, 2019
"""

import sys, codecs, re
from argparse import ArgumentParser,RawTextHelpFormatter

sys.stdout=codecs.getwriter('utf8')(sys.stdout)
sys.stderr=codecs.getwriter('utf8')(sys.stderr)

def main():
	parser = ArgumentParser(description = __doc__, formatter_class=RawTextHelpFormatter)
	parser.add_argument('input', help = 'elan file')
	opts = parser.parse_args()

	txtier = False
	wordcount = 0
	speakerCounts = {}
	speaker = ''

	for line in codecs.open(opts.input, 'r', 'utf8'):
		line = line.strip('\n')
		if "<TIER " in line:
			# change this value if you want a different tier type
			if 'LINGUISTIC_TYPE_REF="transcription"' in line:
				txtier = True
				speakerregex = re.search('PARTICIPANT="([^"]*)"',line)
				if speakerregex:
					speaker = speakerregex.group(1)
					if speaker not in speakerCounts:
						speakerCounts[speaker] = 0
				else:
					print >> sys.stderr, sys.argv[1]
					print >> sys.stderr, line
			else:
				txtier = False

		if txtier:
			if "ANNOTATION_VALUE" in line:
				regex = re.search('<ANNOTATION_VALUE>(.*)<\/ANNOTATION_VALUE',line)
				if regex:
					utt = regex.group(1)
					uttwordcount = len(utt.split(' '))
					wordcount += uttwordcount
					if speakerregex:
						speakerCounts[speaker] += uttwordcount
	for speaker in speakerCounts:
		print (sys.argv[1])+'\t'+speaker+'\t'+str(speakerCounts[speaker])
	print(sys.argv[1])+'\t''TOTAL'+'\t'+str(wordcount)

if __name__=="__main__":
	main()
